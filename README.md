# 2023_03_civ

![](https://gitlab.com/somobu/2023_03_civ/-/raw/master/doc/preview.png?inline=false)


## Что это?

Демка сетевой стратежки. WIP, вряд ли будет завершена.


## Как пощупать?

1. Скачать исходники, при необходимости - распаковать;
2. Скачать [Godot](https://godotengine.org/download) v4.0.1, при необходимости - установить;
3. Запустить Godot и импортировать проект из папки `game/`;
4. Наслаждаться. PRs are welcome.


## Лицензия

[GNU GPL v3](LICENSE.md)

