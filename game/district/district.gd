class_name District
extends Node3D


var type: String = "field"
var depletion_percent: float = 0.0
var damage_taken: float = 0.0
var tasks_enabled = []
var tasks_config = {}

@onready var root = get_node("/root/Root") as Root
@onready var acts_now = get_node("/root/Root/ActionsNow") as ActionsNow


# State

func get_state():
	var state = {}
	state["type"] = type
	state["position"] = "%.3f:%.3f:%.3f" % [position.x, position.y, position.z]
	state["depletion"] = depletion_percent
	state["tasks_enabled"] = tasks_enabled
	state["tasks_config"] = tasks_config
	
	return state

func set_state(state: Dictionary):
	type = state["type"]
	
	var pos = (state["position"] as String).split(":")
	position = Vector3(float(pos[0]), float(pos[1]), float(pos[2]))
	
	depletion_percent = state["depletion"]
	tasks_enabled = state["tasks_enabled"]
	tasks_config = state["tasks_config"]
	
	calc_visuals()


# Visuals

func set_lod_level(level: int):
	var target = null
	
	if level == 1:		target = get_node_or_null("LodNearest")
	elif level == 2:	target = get_node_or_null("LodNear")
	elif level == 3:	target = get_node_or_null("LodFar")
	elif level == 4:	target = get_node_or_null("LodFurther")
	
	if target != null:
		hide_lods()
		target.show()

func hide_lods():
	for node in get_children():
		if node.name.begins_with("Lod"):
			node.hide()

func calc_visuals():
	$LodFar.texture = load(get_config()["icon"])

func get_district_type() -> String:
	return type

func get_district_name() -> String:
	return get_config()["name"]


# Tasks

func get_tasks() -> Array:
	var cfg = get_config()
	if not "tasks" in cfg: return []
	else: return cfg["tasks"]

func get_task_cfg(task_id: String) -> Dictionary:
	return root.cfg_tasks[task_id]

func get_enabled_tasks() -> Array:
	return tasks_enabled

func enable_all_tasks() -> void:
	tasks_enabled = get_tasks()

func enable_task(task_id: String) -> void:
	if task_id in get_tasks() and not task_id in tasks_enabled:
		tasks_enabled.append(task_id)

func disable_task(task_id: String) -> void:
	tasks_enabled.erase(task_id)


# Other

func get_dist_id() -> int:
	return int(name.substr(1)) - 1

func get_config() -> Dictionary:
	return root.cfg_districts[type].duplicate(true)

func get_hold_cost() -> Resources:
	var res = Resources.new()
	res.from_dict(get_config()["hold_cost"])
	return res

func get_production_stats() -> Resources:
	var res = Resources.new()
	
	for task_id in get_enabled_tasks():
		res.add_dict(get_task_cfg(task_id)["prod"])
	
	return res

func is_depletable() -> bool:
	var cfg = get_config()
	if not "depletable" in cfg: return false
	else: return cfg["depletable"]

func get_depletion() -> float:
	return depletion_percent

func get_depletion_speed() -> float:
	return -0.05


#  UI

func get_task_state_action(task_id: String) -> ActionDistTaskState:
	var cell = get_parent() as Cell
	
	for child in acts_now.get_children():
		if child is ActionDistTaskState:
			var action = child as ActionDistTaskState
			if action.cell_id != cell.cell_id: continue
			if action.dist_id != get_dist_id(): continue
			if action.task_id != task_id: continue
			
			return action
	
	return null

func get_enabled_tasks_ui() -> Array:
	var was_enabled = get_enabled_tasks()
	var enabled = []
	
	for task_id in get_tasks():
		var action = get_task_state_action(task_id)
		if action == null:
			if task_id in was_enabled: enabled.append(task_id)
		else:
			if action.is_enabled: enabled.append(task_id)
	
	return enabled

func get_production_stats_ui() -> Resources:
	var res = Resources.new()
	
	for task_id in get_enabled_tasks_ui():
		res.add_dict(get_task_cfg(task_id)["prod"])
	
	res.sub(get_hold_cost())
	
	return res



