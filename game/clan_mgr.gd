class_name ClanMgr
extends Node

const clan_res = preload("res://clan.tscn")
const clan_name_fmt = "Clan#%d"

var max_clan_id = -1


# State

func get_state() -> Dictionary:
	var state = {}
	state["max_clan"] = max_clan_id
	
	var clans = {}
	for clan in get_children():
		clans[clan.clan_id] = clan.get_state()
	
	state["clans"] = clans
	
	return state


func set_state(state: Dictionary):
	max_clan_id = state["max_clan"]
	
	for node in get_children():
		node.queue_free()
		remove_child(node)
	
	var clans = state["clans"]
	for id in clans:
		var instance = clan_res.instantiate()
		instance.name = clan_name_fmt % int(id)
		instance.clan_id = int(id)
		add_child(instance)
		instance.set_state(clans[id])

func reset_state():
	max_clan_id = -1
	
	for node in get_children():
		node.queue_free()
		remove_child(node)


# Other

func get_max_clan_id():
	return max_clan_id


func get_clan(id: int) -> Clan:
	return get_node_or_null(clan_name_fmt % id)


func add_clan(id: int, title: String, color: Color):
	var clan = clan_res.instantiate() as Clan
	clan.name = clan_name_fmt % id
	add_child(clan)
	max_clan_id += 1
	
	clan.clan_id = id
	clan.clan_name = title
	clan.clan_color = color
	
	clan.resources.stone = 10
	clan.resources.ore = 10
	clan.resources.plant = 10
	clan.resources.animal = 10


func remove_clan(id: int):
	var clan = get_node_or_null(clan_name_fmt % id)
	if clan != null:
		remove_child(clan)
		clan.queue_free()


