class_name Root
extends Node3D

signal next_turn


const PORT = 4221

const day_length_in_turns = 17

var cfg_districts	= read_config("districts")
var cfg_tasks		= read_config("tasks")
var cfg_cell_names	= read_config("cell_names")


# State

var current_turn_no = 2:
	set(value):
		current_turn_no = value
		$Player.update_datetime_label()

var current_clan = 0


# Server

var offered_player_ids = []


@onready var field = $Field as Field
@onready var act_mgr = $ActionMgr as ActionMgr


func _ready():
	multiplayer.connect("peer_connected", server_on_peer_connected)
	multiplayer.connect("peer_disconnected", server_on_peer_disconnected)
	multiplayer.connect("connected_to_server", client_on_connected)
	multiplayer.connect("server_disconnected", client_on_disconnected)
	
	if DisplayServer.get_name() == "headless":
		print("Automatically starting dedicated server.")
		start_as_host.call_deferred()
	else:
		show_connect()


# State

func get_state() -> Dictionary:
	var state = {}
	
	state["turn"] = current_turn_no
	state["cells"] = $Field.get_state()
	state["clans"] = $ClanMgr.get_state()
	state["actions"] = $ActionMgr.get_state()
	
	return state

func set_state(state: Dictionary):
	
	# Note: load order matters here
	current_turn_no = state["turn"]
	$ClanMgr.set_state(state["clans"])
	$Field.set_state(state["cells"])
	$ActionMgr.set_state(state["actions"])
	
	emit_signal("next_turn")

func reset_state():
	current_turn_no = 1
	$ClanMgr.reset_state()
	$Field.reset_state()
	$ActionMgr.reset_state()

func save_state():
	var state = get_state()
	var json = JSON.stringify(state, "\t")
	var file = FileAccess.open("user://save.json", FileAccess.WRITE)
	file.store_string(json)
	file.flush()
	file.close()

func load_state():
	var file = FileAccess.open("user://save.json", FileAccess.READ)
	var json = file.get_as_text()
	var state = JSON.parse_string(json)
	set_state(state)


# Connection

func start_as_host():
	show_loading()
	reset_state()
	
	var peer = ENetMultiplayerPeer.new()
	peer.create_server(PORT)
	
	if peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		$ConnectUI/Error.text = "Failed to start multiplayer server"
		return
	
	multiplayer.multiplayer_peer = peer
	
	add_clan_w_color("Host", Color.ALICE_BLUE)
	
	emit_signal("next_turn")
	
	hide_all()

func server_on_peer_connected(_id: int):
	pass

func server_on_peer_disconnected(id: int):
	for clan_r in $ClanMgr.get_children():
		var clan = clan_r as Clan
		if clan.internal_owner_id == id:
			delete_clan(clan.clan_id)
	
	check_next_turn()

## Tells server "hello, my uuid is such"
@rpc("any_peer") func server_greet(_uuid: String):
	var id = multiplayer.get_remote_sender_id()
	
	var uuid_is_bound = false # TODO: implement me?
	
	if uuid_is_bound:
		rpc_id(id, "client_bind_to_clan", 4221)
		rpc_id(id, "client_update_state", get_state())
	else:
		rpc_id(id, "client_showclanselect")

@rpc("any_peer") func server_add_actions(actions: Array):
	var id = multiplayer.get_remote_sender_id()
	if not id in offered_player_ids:
		offered_player_ids.append(id)
		($ActionMgr as ActionMgr).offer_actions(actions)
		check_next_turn()


func start_as_client():
	var txt : String = $ConnectUI/Picker/Address.text
	if txt == "":
		$ConnectUI/Error.text = "Need a remote address to connect to"
		return
	
	var peer = ENetMultiplayerPeer.new()
	peer.create_client(txt, PORT)
	
	# This part is broken :/
	if peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		$ConnectUI/Error.text = "Failed to start multiplayer client"
		print("Got conn error")
		return
	
	multiplayer.multiplayer_peer = peer

func client_on_connected():
	rpc_id(1, "server_greet", "4221")

func client_on_disconnected():
	show_connect()

@rpc func client_showclanselect():
	show_clanselect()

@rpc func client_bind_to_clan(clan_id: int):
	current_clan = clan_id

@rpc func next_turn_state(state: Dictionary):
	set_state(state)
	hide_all()
	$Player.unlock_turn_button()


func show_connect():
	hide_all()
	$ConnectUI.show()

func show_loading():
	hide_all()
	$Loading.show()

func show_clanselect():
	hide_all()
	$ClanSelect.show()

func show_wait_turn():
	hide_all()
	$WaitTurn.show()

func hide_all():
	$ConnectUI.hide()
	$Loading.hide()
	$ClanSelect.hide()
	$WaitTurn.hide()


func on_clanselect_done():
	var newclan = load("res://actions/action_new_clan.tscn").instantiate() as ActionNewClan
	newclan.player_id = multiplayer.get_unique_id()
	newclan.clan_name = $ClanSelect/Panel/LineEdit.text
	newclan.clan_color = $ClanSelect/Panel/ColorPickerButton.color
	
	rpc_id(1, "server_add_actions", [ newclan.get_state() ])
	
	newclan.queue_free()
	
	$Player.lock_turn_button()
	show_wait_turn()


# Other

func read_config(filename: String):
	var file = FileAccess.open("res://config/%s.json" % filename, FileAccess.READ)
	return JSON.parse_string(file.get_as_text())

func check_next_turn():
	for player_id in multiplayer.get_peers():
		if not player_id in offered_player_ids:
			print("%s hasnt done their turn yet" % player_id)
			return
	
	# Host is player too
	if not 1 in offered_player_ids: return
	
	# All players done their turns:
	do_next_turn()

func do_next_turn():
	if multiplayer.is_server():
		offered_player_ids.clear()
		
		current_turn_no += 1
		
		act_mgr.process_action_type(ActionMgr.CN_DIST_TASK_STATE)
		field.turn_cell_production()
		field.turn_cell_hold_cost()
		# civils movement
		# district (task) special actions: per-turn/task-finish
		# district transforms
		field.turn_cell_removal()
		act_mgr.process_action_type(ActionMgr.CN_NEW_CLAN)
		
		var state = get_state()
		for peer in multiplayer.get_peers():
			rpc_id(peer, "next_turn_state", state)
		
		emit_signal("next_turn")

func we_done_our_turn():
	var our_actions = $ActionsNow.get_actions()
	
	if multiplayer.is_server():
		offered_player_ids.append(1)
		($ActionMgr as ActionMgr).offer_actions(our_actions)
		check_next_turn()
	else:
		rpc_id(1, "server_add_actions", our_actions)
	
	$ActionsNow.clear()


func add_clan_w_color(clan_name: String, clan_color: Color) -> int:
	var clan_id = $ClanMgr.get_max_clan_id() + 1
	$ClanMgr.add_clan(clan_id, clan_name, clan_color)
	$Field.allocate_new_cells(clan_id)
	
	return clan_id

func delete_clan(id):
	$Field.unown_cells(id)
	$ClanMgr.remove_clan(id)
	$Field.slaughter_cells(7+30) # TODO: dehardcode me


