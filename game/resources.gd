class_name Resources
extends Object

var stone = 0.0;
var ore = 0.0;
var plant = 0.0;
var animal = 0.0;

var metal = 0.0;
var commod = 0.0;
var weapon = 0.0;
var knowledge = 0.0;


func to_dict() -> Dictionary:
	return {
		"stone": stone,
		"ore": ore,
		"plant": plant,
		"animal": animal,
		"metal": metal,
		"commod": commod,
		"weapon": weapon,
		"knowledge": knowledge
	}


func from_dict(res: Dictionary):
	if res == null: return
	
	if "stone" in res:		stone		= res["stone"]
	if "ore" in res:		ore			= res["ore"]
	if "plant" in res:		plant		= res["plant"]
	if "animal" in res:		animal		= res["animal"]
	if "metal" in res:		metal		= res["metal"]
	if "commod" in res:		commod		= res["commod"]
	if "weapon" in res:		weapon		= res["weapon"]
	if "knowledge" in res:	knowledge	= res["knowledge"]



func add(res: Resources):
	stone		+= res.stone
	ore			+= res.ore
	plant		+= res.plant
	animal		+= res.animal
	metal		+= res.metal
	commod		+= res.commod
	weapon		+= res.weapon
	knowledge	+= res.knowledge

func add_dict(dict: Dictionary):
	var res = Resources.new()
	res.from_dict(dict)
	add(res)

func sub(res: Resources):
	stone		-= res.stone
	ore			-= res.ore
	plant		-= res.plant
	animal		-= res.animal
	metal		-= res.metal
	commod		-= res.commod
	weapon		-= res.weapon
	knowledge	-= res.knowledge

func sub_dict(dict: Dictionary):
	var res = Resources.new()
	res.from_dict(dict)
	sub(res)
