class_name Field
extends Node3D

const cell_ref = preload("res://cell.tscn")

const step_x = 1.0
const step_y = 0.867

const init_cell_clan = 7
const init_cell_free = 30

const cells_to_radius = {
	43: 3.5,
	55: 3.7,
	57: 4.0,
	61: 4.1,
	73: 4.4,
	85: 4.6,
	87: 5.0,
	91: 5.1,
	95: 5.2,
	109: 5.3,
	121: 5.6,
	123: 6.0,
	139: 6.1,
	151: 6.3,
	163: 6.6,
	171: 7.0,
	187: 7.1,
	199: 7.3,
	211: 7.6,
	223: 7.9,
	237: 8.0,
	241: 8.1,
	253: 8.2,
	265: 8.6,
	271: 8.7,
	283: 8.8,
	295: 8.9,
	297: 9.0,
	301: 9.1,
	313: 9.2,
	337: 9.6,
	349: 9.7,
	361: 9.9,
	363: 10.0,
	367: 10.1,
	379: 10.2,
	383: 10.4,
	397: 10.5,
	421: 10.6,
	433: 10.9,
	435: 11.0,
	439: 11.1,
	451: 11.2,
	463: 11.3,
	475: 11.4,
	499: 11.6,
	507: 11.8,
	511: 11.9,
	513: 12.0,
	517: 12.1,
	547: 12.2,
	555: 12.3,
	559: 12.4,
	567: 12.5,
	583: 12.6,
	595: 12.8,
	597: 13.0,
	625: 13.1,
	637: 13.2,
	649: 13.3,
	661: 13.5,
	673: 13.6,
	685: 13.8,
	699: 13.9,
	705: 14.0,
	721: 14.1,
	745: 14.2,
	757: 14.5,
	769: 14.6,
	793: 14.8,
	805: 14.9,
	819: 15.0,
	827: 15.1,
	847: 15.2,
	851: 15.4,
	859: 15.5,
	875: 15.6,
	889: 15.7,
	913: 15.8,
	925: 15.9,
	927: 16.0,
	939: 16.1,
	955: 16.2,
	967: 16.4,
	979: 16.5,
	1003: 16.6,
	1015: 16.7,
	1027: 16.8,
	1039: 16.9,
	1041: 17.0,
	1061: 17.1,
	1069: 17.2,
	1099: 17.4,
	1111: 17.5,
	1135: 17.6,
	1139: 17.7,
	1159: 17.8,
	1161: 18.0,
	1189: 18.1,
	1197: 18.2,
	1213: 18.3,
	1237: 18.4,
	1261: 18.6,
	1273: 18.7,
	1285: 18.8,
	1287: 19.0,
	1329: 19.1,
	1345: 19.2,
	1353: 19.3,
	1369: 19.4,
	1381: 19.5,
	1393: 19.6,
	1409: 19.7,
	1417: 19.8 }


var last_cell_id = 0
var last_hovered_cell = null


@onready var root = get_node("/root/Root") as Root
@onready var clan_mgr = get_node("/root/Root/ClanMgr") as ClanMgr

# State

func get_state() -> Dictionary:
	var state = {}
	state["last_cell"] = last_cell_id
	
	var cells = {}
	for cell in get_children():
		cells[cell.cell_id] = cell.get_state()
	state["cells"] = cells
	
	return state

func set_state(state: Dictionary):
	last_cell_id = state["last_cell"]
	
	for child in get_children():
		child.workaround_queue_free()
		child.queue_free()
		remove_child(child)
	
	for cell_id in state["cells"]:
		var instance = cell_ref.instantiate() as Cell
		instance.name = "C%s" % str(cell_id)
		instance.cell_id = cell_id
		add_child(instance)
		instance.set_state(state["cells"][cell_id])

func reset_state():
	last_cell_id = 0
	
	for child in get_children():
		child.workaround_queue_free()
		child.queue_free()
		remove_child(child)


# Turn

func turn_cell_removal() -> void:
	for child in get_children():
		var cell = child as Cell
		cell.tick_del_countdown()
		if cell.delete_me_now():
			cell.workaround_queue_free()
			cell.queue_free()
			remove_child(cell)

func turn_cell_production() -> void:
	for child in get_children():
		var cell = child as Cell
		
		if not cell.is_unowned():
			var clan = clan_mgr.get_clan(cell.get_owner_id())
			var prod = cell.get_overall_production_stats()
			clan.resources.add(prod)
		

func turn_cell_hold_cost() -> void:
	for child in get_children():
		var cell = child as Cell
		
		if not cell.is_unowned():
			var clan = clan_mgr.get_clan(cell.get_owner_id())
			var prod = cell.get_hold_cost_stats()
			clan.resources.sub(prod)


# Other

func allocate_new_cells(new_clan_id: int):
	var target_cell_count = get_child_count() + init_cell_clan + init_cell_free
	
	var radius = 3.3
	for cells in cells_to_radius:
		if cells > target_cell_count:
			radius = cells_to_radius[cells] * 1.2
			break
	
	var free_spaces = []
	var free_groups = []
	var groups_by_size = {}
	
	
	# Find out unoccupied cell positions
	var m_y = ceili(radius * (1/step_y))
	var m_x = ceili(radius * (1/step_y))
	
	for y in range(-m_y, m_y):
		for x in range(-m_x, m_x):
			var x_ofs = 0.0 if y % 2 == 0 else 0.5
			var gpos = Vector3(x + x_ofs, 0, y * step_y)
			
			if gpos.length() > radius: continue
			
			var is_free_pos = true
			for node in get_children():
				if (node.position - gpos).length() < 0.1:
					is_free_pos = false
					break
			
			if is_free_pos:
				free_spaces.push_back(gpos)
				free_groups.push_back(-1)
	
	assert(free_spaces.size() >= (init_cell_clan + init_cell_free) )
	
	# Assign adjacency-based cell groups for that positions
	var last_new_group = -1
	for i in range(free_spaces.size()):
		var our_id = -1
		for j in range(free_spaces.size()):
			var is_adjacent = (free_spaces[i] - free_spaces[j]).length() < (step_x*1.1)
			var their_id = free_groups[j]
			
			if is_adjacent and their_id != -1:
				if our_id == -1:
					our_id = their_id
					free_groups[i] = their_id
				else:
					for k in range(free_groups.size()):
						if free_groups[k] == their_id:
							free_groups[k] = our_id
		
		if our_id == -1:
			last_new_group += 1
			free_groups[i] = last_new_group
	
	
	# Calculate groups sizes
	for i in range(free_spaces.size()):
		var group_id = free_groups[i]
		
		if not group_id in groups_by_size.keys():
			var group_count = 0
			for j in range(free_spaces.size()):
				if free_groups[j] == group_id:
					group_count += 1
			
			groups_by_size[group_id] = group_count
	
	
	# Actually spawn cells
	var our_spawned_cells = []
	var current_group = -1
	
	for i in range(init_cell_clan):
		var cells_left_in_group = 0 if current_group == -1 else groups_by_size[current_group]
		
		# No cells left in current group, lets find next biggest
		if cells_left_in_group < 1:
			for group in groups_by_size:
				if groups_by_size[group] > cells_left_in_group:
					cells_left_in_group = groups_by_size[group]
					current_group = group
			
			assert(cells_left_in_group > 0)
			
			# Lets start with random cell in group
			var rand_group_cell = randi_range(0, cells_left_in_group)
			for j in range(2 * free_spaces.size()):
				var idx = j % free_spaces.size()
				if free_groups[idx] == current_group:
					rand_group_cell -= 1
				
				if rand_group_cell == 0:
					var cell = spawn_cell(free_spaces[idx], new_clan_id)
					our_spawned_cells.push_back(cell)
					free_spaces.remove_at(idx)
					free_groups.remove_at(idx)
					groups_by_size[current_group] -= 1
					break
		
		else:
			var rand_offset = randi_range(0, free_spaces.size())
			var cell_spawned = false
			for raw_j in range(free_spaces.size()):
				var j = (raw_j+rand_offset) % free_spaces.size()
				
				if cell_spawned: break
				if free_groups[j] != current_group: continue
				
				for our_cell in our_spawned_cells:
					var is_adjacent = (free_spaces[j] - our_cell.position).length() < (step_x*1.1)
					if is_adjacent:
						var cell = spawn_cell(free_spaces[j], new_clan_id)
						our_spawned_cells.push_back(cell)
						free_spaces.remove_at(j)
						free_groups.remove_at(j)
						groups_by_size[current_group] -= 1
						cell_spawned = true
						break
			
			if not cell_spawned:
				print("CARAMBA!")
	
	
	current_group = -1
	
	for i in range(init_cell_free):
		var cells_left_in_group = 0 if current_group == -1 else groups_by_size[current_group]
		
		# No cells left in current group, lets find next smallest
		if cells_left_in_group < 1:
			cells_left_in_group = 999999
			
			for group in groups_by_size:
				if groups_by_size[group] < 1: continue
				if groups_by_size[group] < cells_left_in_group:
					cells_left_in_group = groups_by_size[group]
					current_group = group
			
			assert(cells_left_in_group > 0)
			assert(cells_left_in_group != 999999)
		
		
		# Lets pick random cell in group
		var rand_group_cell = randi_range(0, cells_left_in_group)
		for j in range(2 * free_spaces.size()):
			var idx = j % free_spaces.size()
			if free_groups[idx] == current_group:
				rand_group_cell -= 1
			
			if rand_group_cell == 0:
				spawn_cell(free_spaces[idx], -1)
				free_spaces.remove_at(idx)
				free_groups.remove_at(idx)
				groups_by_size[current_group] -= 1
				break

func slaughter_cells(count: int):
	for i in range(count):
		var idx = randi_range(0, get_child_count() - 1)
		var child = get_child(idx)
		child.set_deletion_countdown(3)

func spawn_cell(gpos, owner_id = 0):
	last_cell_id += 1
	
	var instance = cell_ref.instantiate() as Cell
	instance.name = "C%d" % last_cell_id
	instance.cell_id = last_cell_id
	instance.cell_name = generate_cell_name()
	add_child(instance, true)
	
	instance.global_position = gpos
	instance.init_districts(randi_range(1, 7))
	if owner_id >= 0: instance.set_cell_owner(owner_id)
	
	return instance

func get_cell(id: int) -> Cell:
	return get_node_or_null("C%d" % id)

func generate_cell_name() -> String:
	var cell_name = root.cfg_cell_names["base"]
	var cell_name_prefix = root.cfg_cell_names["prefix"]
	var cell_name_suffix = root.cfg_cell_names["suffix"]
	
	var gen_cell_name: String = cell_name[randi_range(0, cell_name.size()-1)]
	
	if randf_range(0,1) < 0.8:
		var prefix = cell_name_prefix[randi_range(0, cell_name_prefix.size()-1)]
		gen_cell_name = prefix + " " + gen_cell_name
	
	if randf_range(0,1) < 0.6:
		var suffix = cell_name_suffix[randi_range(0, cell_name_suffix.size()-1)]
		gen_cell_name = gen_cell_name + " " + suffix
	
	return gen_cell_name


func get_owned_cells_count(clan):
	var count = 0
	
	for cell in get_children():
		if cell.cell_owner == clan:
			count += 1
	
	return count


func unown_cells(clan_id):
	for cell in get_children():
		if cell.cell_owner == clan_id:
			cell.set_cell_owner(-1)


func set_lod_level(level: int):
	for cell in get_children():
		cell.set_lod_level(level)

