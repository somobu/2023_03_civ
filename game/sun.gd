extends Node3D

@export var angle: Curve
@export var altitude: Curve
@export var color: Gradient
@export var energy: Curve

@onready var root: Root = get_node("/root/Root")


func on_next_turn():
	update_sun(root.current_turn_no)


func update_sun(turn_no: int):
	var day_length_in_turns = root.day_length_in_turns
	var day_percent = float(turn_no % day_length_in_turns) / day_length_in_turns
	if day_percent == 0: day_percent = 0.01
	
	rotation.y = angle.sample(day_percent) * (-PI)
	$Light.rotation.x = altitude.sample(day_percent) * (-PI/2)
	$Light.light_color = color.sample(day_percent)
	$Light.light_energy = energy.sample(day_percent)
	$Light.light_indirect_energy = energy.sample(day_percent)
	
