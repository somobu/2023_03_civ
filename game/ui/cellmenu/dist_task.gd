extends Button

signal task_state_changed(task_id: String, is_enabled: bool)


@onready var root = get_node("/root/Root") as Root

var my_id = null


func assign_task(task_id: String, active: bool):
	button_pressed = active
	my_id = task_id
	
	var task_data = root.cfg_tasks[task_id]
	
	$Title.text = task_data["name"]
	
	if "prod" in task_data:
		var res_idx = 1
		for res_id in task_data["prod"]:
			var icon = get_node("Img_%d" % res_idx)
			var text = get_node("Val_%d" % res_idx)
			var value = task_data["prod"][res_id]
			
			icon.texture = get_icon(res_id)
			text.text = "%+.1f" % value
			
			make_colored(text, icon, value)
			
			res_idx += 1
		
		while res_idx <= 4:
			get_node("Img_%d" % res_idx).texture = null
			get_node("Val_%d" % res_idx).text = ""
			res_idx += 1


func get_icon(resource_id: String) -> Texture:
	match resource_id:
		"animal":		return load("res://ui/icons/res_animal.png")
		"commod":		return load("res://ui/icons/res_commodity.png")
		"knowledge":	return load("res://ui/icons/res_knowledge.png")
		"metal":		return load("res://ui/icons/res_metal.png")
		"ore": 			return load("res://ui/icons/res_ore.png")
		"plant":		return load("res://ui/icons/res_plant.png")
		"stone":		return load("res://ui/icons/res_stone.png")
		"weapon":		return load("res://ui/icons/res_weapon.png")
	return null


func make_colored(node: Control, icon: Control, value: float):
	var color = Color.GRAY
	
	if value > 0.01:	color = Color.LIGHT_GREEN
	elif value < -0.01:	color = Color.PALE_VIOLET_RED
	
	node.self_modulate = color
	icon.self_modulate = color


func on_button_up():
	emit_signal("task_state_changed", my_id, button_pressed)
