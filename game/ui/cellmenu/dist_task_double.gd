extends Panel

signal task_state_changed(task_id: String, is_enabled: bool)


func assign_task(task_id: String, active: bool, is_left: bool) -> void:
	if is_left: $Left.assign_task(task_id, active)
	else: $Right.assign_task(task_id, active)

func clear_right() -> void:
	$Right.visible = false


func state_changed(task_id, is_enabled, is_left):
	emit_signal("task_state_changed", task_id, is_enabled)
