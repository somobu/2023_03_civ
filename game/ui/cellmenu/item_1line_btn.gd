@tool
extends Control

signal on_action_press

@export var text_label = "1liner btn yay"
@export var text_btn = "1LINE"

func _ready():
	$Label.text = text_label
	$Button.text = text_btn

func on_button_up():
	emit_signal("on_action_press")
