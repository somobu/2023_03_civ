@tool
extends Control

signal on_action_press

@export var text = "Dat act btn"

func _ready():
	$Button.text = text

func on_button_up():
	emit_signal("on_action_press")
