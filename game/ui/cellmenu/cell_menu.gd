extends Control

signal menu_gonna_close

var district_container_ref = preload("res://ui/cellmenu/district_root.tscn")
var district_task_2_ref = preload("res://ui/cellmenu/dist_task_double.tscn")

@onready var root = get_node("/root/Root") as Root
@onready var acts_now = get_node("/root/Root/ActionsNow") as ActionsNow

var current_cell: Cell = null


func reset_cell_ptrs():
	current_cell = null


func switch_cell(cell: Cell):
	current_cell = cell
	var start = Time.get_ticks_msec()
	redraw_overview()
	redraw_right()
	var end = Time.get_ticks_msec()
	print("Cell menu built in %d msec" % [end-start])


func redraw_overview():
	var container = $Tabs/Overview/ScrollContainer/VBoxContainer
	for child in container.get_children(): child.queue_free()
	
	
	var cell = current_cell
	if cell == null:
		return
	
	
	var is_owned = not cell.is_unowned()
	var is_owned_by_me = cell.is_owned_by(root.current_clan)
	
	add_to_overview(get_header(cell.cell_name))
	
	if is_owned_by_me:	add_to_overview(get_1liner("Owned by you"))
	elif is_owned:		add_to_overview(get_1liner("Owned by %s" % cell.get_owner_name()))
	else: 				add_to_overview(get_1liner("Currently unowned"))
	
	if cell.is_in_conflict():
		add_to_overview(get_warning("In conflict"))
	
	if cell.is_losing_owner():
		add_to_overview(get_warning("Will loose owner in 3 turns"))
	
	if cell.will_be_deleted():
		add_to_overview(get_warning("Will be removed in %d turns" % cell.get_deleteion_countdown()))
	
	add_to_overview(get_spacer())
	
	if is_owned_by_me:
		add_to_overview(get_1liner("Hold cost per turn"))
		add_to_overview(get_res(cell.get_hold_cost_stats(), true))
	
	var net_total = get_res(cell.get_overall_production_stats_ui())
	net_total.signed = true
	add_to_overview(get_1liner("Production over consumption"))
	add_to_overview(net_total)
	add_to_overview(get_spacer())
	
	if cell.get_pops_count_total() > 0:
		add_to_overview(get_1liner("Population (%d/%d):" % [cell.get_pops_count_total(), cell.get_pops_limit()]))
		
		if cell.is_unowned():
			var txt = "   %d unowned civils" % cell.get_civils_count_total()
			add_to_overview(get_1liner(txt))
		elif not is_owned_by_me:
			var txt = "   %d civils of %s" % [ cell.get_civils_count_total(), cell.get_owner_name() ]
			add_to_overview(get_1liner(txt))
		else:
			var txt = "   %d your civils (%d free)" % [ cell.get_civils_count_total(), cell.get_civils_count_free()]
			add_to_overview(get_1liner_btn(txt , "Move"))
		
		var mils_dict = cell.get_mils_count_dict()
		for clan_id in mils_dict:
			if clan_id == root.current_clan:
				add_to_overview(get_1liner_btn("   %d your mils" % mils_dict[clan_id], "Move"))
			else:
				add_to_overview(get_1liner("   %d mils of clan #%d" % [ mils_dict[clan_id], clan_id] ))
		add_to_overview(get_spacer())
	
	if is_owned_by_me:
		add_to_overview(get_action("Swap"))
		add_to_overview(get_action("Abandon"))
	elif is_owned:
		add_to_overview(get_action("Assault"))
		add_to_overview(get_action("Plunder"))
		add_to_overview(get_action("Ruin"))
		add_to_overview(get_action("Seize"))
	else:
		add_to_overview(get_action("Seize"))
	
	add_to_overview(get_spacer())


func redraw_right():
	var container = $Tabs/Districts/Grid
	for child in container.get_children(): child.queue_free()
	
	var is_owned_by_me = current_cell.is_owned_by(root.current_clan)
	
	var districts = current_cell.get_districts()
	for dist_idx in len(districts):
		var district = districts[dist_idx] as District
		
		var dist_root = district_container_ref.instantiate()
		container.add_child(dist_root)
		
		
		var add_to_right = func(node: Control):
			dist_root.add_child(node)
		
		var task_state_changed = func(task_id: String, is_enabled: bool):
			on_district_task_state_change(dist_idx, task_id, is_enabled)
		
		var header = get_header(district.get_district_name()) as Control
		add_to_right.call(header)
		add_to_right.call(get_1liner("Population: 12 civils"))
		
		if district.is_depletable():
			var dep = district.get_depletion() * 100
			var spd = district.get_depletion_speed() * 100
			add_to_right.call(get_1liner("Depletion: %.1f%% (%+.1f%%/turn)" % [dep, spd]))
		
		if is_owned_by_me:
			add_to_right.call(get_spacer())
			add_to_right.call(get_1liner("Prod. over cons."))
			add_to_right.call(get_res(district.get_production_stats_ui()))
		
		
		add_to_right.call(get_spacer())
		
		
		if is_owned_by_me:
			var task_ids = district.get_tasks()
			var enabled_ui_ids = district.get_enabled_tasks_ui()
			
			var enabled = []
			var disabled = []
			
			for task_id in task_ids:
				if task_id in enabled_ui_ids: enabled.append(task_id)
				else: disabled.append(task_id)
			
			var last_container = null
			for idx in len(task_ids):
				var id = task_ids[idx]
				var task_enabled = id in enabled
				var task_name = root.cfg_tasks[id]["name"]
				
				
				if idx % 2 == 0:
					last_container = district_task_2_ref.instantiate()
					add_to_right.call(last_container)
					last_container.connect("task_state_changed", task_state_changed)
					last_container.assign_task(id, task_enabled, true)
				else:
					last_container.assign_task(id, task_enabled, false)
				
			if len(task_ids) % 2 == 1:
				last_container.clear_right()
			
			add_to_right.call(get_spacer())


func get_header(text: String):
	var instance = load("res://ui/cellmenu/item_header.tscn").instantiate()
	instance.text = text
	return instance

func get_spacer():
	return load("res://ui/cellmenu/item_spacer.tscn").instantiate()

func get_spacer_smol():
	return load("res://ui/cellmenu/item_spacer_smol.tscn").instantiate()

func get_1liner(text: String):
	var instance = load("res://ui/cellmenu/item_1line.tscn").instantiate()
	instance.text = text
	return instance

func get_1liner_btn(text_title: String, text_btn: String):
	var instance = load("res://ui/cellmenu/item_1line_btn.tscn").instantiate()
	instance.text_label = text_title
	instance.text_btn = text_btn
	return instance

func get_res(res: Resources, inversed: bool = false):
	var layout = "res://ui/cellmenu/item_resources.tscn"
	
	var instance = load(layout).instantiate()
	
	instance.inversed = inversed
	
	instance.res_stone = res.stone;
	instance.res_ore = res.ore;
	instance.res_plant = res.plant;
	instance.res_animal = res.animal;

	instance.res_metal = res.metal;
	instance.res_commod = res.commod;
	instance.res_weapon = res.weapon;
	instance.res_knowledge = res.knowledge;
	
	return instance

func get_warning(text: String):
	var instance = load("res://ui/cellmenu/item_warning.tscn").instantiate()
	instance.text = text
	return instance

func get_action(text: String):
	var instance = load("res://ui/cellmenu/item_action.tscn").instantiate()
	instance.text = text
	return instance

func get_pops_ctrl(count: int):
	var instance = load("res://ui/cellmenu/item_pops_ctrl.tscn").instantiate()
	instance.pops_cnt = count
	return instance



func add_to_overview(node: Control):
	$Tabs/Overview/ScrollContainer/VBoxContainer.add_child(node)



# Callbacks

func on_close_pressed():
	emit_signal("menu_gonna_close")
	reset_cell_ptrs()

func on_district_task_state_change(dist_idx: int, task_id: String, is_enable: bool):
	var action = load("res://actions/actoion_dist_task_state.tscn").instantiate() as ActionDistTaskState
	action.cell_id = current_cell.cell_id
	action.dist_id = dist_idx
	action.task_id = task_id
	action.is_enabled = is_enable
	
	acts_now.add_action(action)
	redraw_overview()
	redraw_right()

