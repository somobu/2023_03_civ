@tool
extends Control

@export var signed = false
@export var inversed = false

@export var res_stone = 1.3;
@export var res_ore = 0.9;
@export var res_plant = 2.75;
@export var res_animal = 4.22;

@export var res_metal = 2.1;
@export var res_commod = 2.28;
@export var res_weapon = 1.3;
@export var res_knowledge = 3.7;


func _ready():
	
	var format = "%+.1f" if signed else "%.1f"
	
	$Stone.text		= format % res_stone;
	$Ore.text		= format % res_ore;
	$Plant.text		= format % res_plant;
	$Animal.text	= format % res_animal;
	
	$Metal.text		= format % res_metal;
	$Comm.text		= format % res_commod;
	$Weapon.text	= format % res_weapon;
	$Knowledge.text	= format % res_knowledge;
	
	make_colored($Stone, $StoneIcon, res_stone)
	make_colored($Ore, $OreIcon, res_ore)
	make_colored($Plant, $PlantIcon, res_plant)
	make_colored($Animal, $AnimalIcon, res_animal)
	
	make_colored($Metal, $MetalIcon, res_metal)
	make_colored($Comm, $CommIcon, res_commod)
	make_colored($Weapon, $WeaponIcon, res_weapon)
	make_colored($Knowledge, $KnowledgeIcon, res_knowledge)


func make_colored(node: Control, icon: Control, value: float):
	var color = Color.GRAY
	
	if not inversed:
		if value > 0.01:	color = Color.LIGHT_GREEN
		elif value < -0.01:	color = Color.PALE_VIOLET_RED
	else:
		if value > 0.01:	color = Color.PALE_VIOLET_RED
		elif value < -0.01:	color = Color.LIGHT_GREEN
	
	node.self_modulate = color
	icon.self_modulate = color
