class_name Action
extends Node

var player_id = -1
var turn_no = -1


# State

func get_state() -> Dictionary:
	return {
		"player": player_id,
		"turn": turn_no,
		"cname": get_cname()
	}

func set_state(state: Dictionary):
	player_id = state["player"]
	turn_no = state["turn"]


# Override me

## Workaround - we cannot get script's class name yet (#21789)
func get_cname() -> String:
	return ActionMgr.CN_ACTION

## Merge/update/replace this action with given one (if applicable)
## Returns true if given action was successfully merged 
##  (and therefore given action should be merged to any other)
func merge(_action: Action) -> bool:
	return false

## Performs action, that is
func perform() -> void:
	pass


# Data access

func get_player_id() -> int:
	return player_id

func get_turn_no() -> int:
	return turn_no
