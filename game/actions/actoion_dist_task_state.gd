class_name ActionDistTaskState
extends Action

var cell_id = 0
var dist_id = 0
var task_id = ""
var is_enabled = false

@onready var field = get_node("/root/Root/Field") as Field

# State

func get_state() -> Dictionary:
	var state = super.get_state()
	state["cell_id"] = cell_id
	state["dist_id"] = dist_id
	state["task_id"] = task_id
	state["enable"] = is_enabled
	return state

func set_state(state: Dictionary):
	super.set_state(state)
	cell_id = state["cell_id"]
	dist_id = state["dist_id"]
	task_id = state["task_id"]
	is_enabled = state["enable"]


# Overrides

func get_cname() -> String:
	return ActionMgr.CN_DIST_TASK_STATE

func merge(action: Action) -> bool:
	if not action is ActionDistTaskState: return false
	
	var casted = action as ActionDistTaskState
	
	if casted.cell_id != cell_id: return false
	if casted.dist_id != dist_id: return false
	if casted.task_id != task_id: return false
	
	return true

func perform() -> void:
	var cell: Cell = field.get_cell(cell_id)
	
	if cell == null:
		push_error("Unable to get cell id %d" % cell_id)
		return
	
	var dist: District = cell.get_district(dist_id)
	
	if is_enabled:
		dist.enable_task(task_id)
	else:
		dist.disable_task(task_id)
