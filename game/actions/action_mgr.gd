class_name ActionMgr
extends Node

const CN_ACTION = "action"
const CN_NEW_CLAN = "new_clan"
const CN_DIST_TASK_STATE = "dist_task_state"


# State

func get_state() -> Dictionary:
	var actions = []
	
	for action in get_children():
		var state = action.get_state()
		actions.push_back(state)
	
	return {
		"actions": actions
	}

func set_state(state: Dictionary):
	for child in get_children():
		remove_child(child)
		child.queue_free()
	
	for action_entry in state["actions"]:
		var instance = cname_to_instance(action_entry["cname"])
		add_child(instance)
		instance.set_state(action_entry)

func reset_state():
	for child in get_children():
		remove_child(child)
		child.queue_free()

func cname_to_instance(cname: String) -> Node:
	match cname:
		CN_ACTION:			return load("res://actions/action.tscn").instantiate()
		CN_NEW_CLAN:		return load("res://actions/action_new_clan.tscn").instantiate()
		CN_DIST_TASK_STATE:	return load("res://actions/actoion_dist_task_state.tscn").instantiate()
	
	return null


# Other

func offer_actions(actions: Array):
	for action_entry in actions:
		var instance = cname_to_instance(action_entry["cname"])
		add_child(instance)
		instance.set_state(action_entry)
		push_warning("TODO: check for merge-able actions!")

func process_action_type(type: String):
	for action in get_children():
		if action.get_cname() == type:
			process_action(action)

func process_action(action: Action):
	if action.player_id == -1:
		push_error("Got player_id for action below -- forgot to call super?", action.get_state())
	else:
		action.perform()
	
	action.queue_free()
	remove_child(action)
