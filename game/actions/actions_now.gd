class_name ActionsNow
extends Node


@onready var root = get_node("/root/Root") as Root

func add_action(action: Action) -> void:
	action.player_id = multiplayer.get_unique_id()
	action.turn_no = root.current_turn_no
	
	for child in get_children():
		var merged = action.merge(child)
		if merged:
			child.queue_free()
			remove_child(child)
	
	add_child(action)


func get_actions() -> Array:
	var actions = []
	
	for action in get_children():
		var state = action.get_state()
		actions.push_back(state)
	
	return actions


func clear() -> void:
	for child in get_children():
		child.queue_free()
		remove_child(child)
