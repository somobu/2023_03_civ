class_name ActionNewClan
extends Action

var clan_name = "DatClan"
var clan_color = Color.DARK_RED


# State

func get_state() -> Dictionary:
	var state = super.get_state()
	state["clan_name"] = clan_name
	state["clan_color"] = clan_color.to_html(false)
	return state

func set_state(state: Dictionary):
	super.set_state(state)
	clan_name = state["clan_name"]
	clan_color = Color.from_string(state["clan_color"], Color.DARK_CYAN)


# Overrides

func get_cname() -> String:
	return ActionMgr.CN_NEW_CLAN

func perform() -> void:
	var root = get_node("/root/Root") as Root
	var new_clan_id = root.add_clan_w_color(clan_name, clan_color)
	
	var new_clan = get_node("/root/Root/ClanMgr").get_clan(new_clan_id) as Clan
	new_clan.internal_owner_id = player_id
	
	root.rpc_id(player_id, "client_bind_to_clan", new_clan_id)
