extends Node3D

const cam_speed = 3.0
const zoom_min = 3
const zoom_max = 40

var in_gui = false
var selected_cell = null

var current_lod_level = -1

var saved_gpos = Vector3(0,0,0)
var saved_zoom = 10


@onready var root = get_node("/root/Root")
@onready var cell_mgr = get_node("/root/Root/Field")
@onready var clan_mgr = get_node("/root/Root/ClanMgr")

func _ready():
	$CellMenu.visible = false
	$Control/ClansList.visible = false
	
	update_stats_view()


func _process(delta):
	var cell = cell_mgr.last_hovered_cell as Cell
	
	check_lod()
	
	# Cell info
	$Control/CellInfo.visible = cell != null
	
	
	if in_gui: return
	
	
	# Movement
	var move = Input.get_vector("move_left", "move_right", "move_fwd", "move_bwd").normalized()
	global_position += Vector3(move.x, 0, move.y) * cam_speed * delta
	
	# Zoom
	var prev_zoom = $CamBase/CamHead/Camera3D.position.z
	var zoom = Input.get_axis("zoom_in", "zoom_out") * cam_speed * delta
	$CamBase/CamHead/Camera3D.position.z = clamp(prev_zoom + zoom, zoom_min, zoom_max)
	
	# Cell selection
	if cell != null and $CellSelectCooldown.is_stopped():
		$Control/CellInfo/Label.text = cell.describe_short()
		
		if Input.is_action_just_released("cell_select"):
			show_cell_menu(cell)
			cell_mgr.last_hovered_cell = null


func check_lod():
	var zoom_level = $CamBase/CamHead/Camera3D.position.z
	var target_lod_level = -1
	
	if zoom_level < 3.5:	target_lod_level = 1
	elif zoom_level < 6.5:	target_lod_level = 2
	elif zoom_level < 10:	target_lod_level = 3
	else:					target_lod_level = 4
	
	if current_lod_level != target_lod_level:
		current_lod_level = target_lod_level
		cell_mgr.set_lod_level(target_lod_level)


func on_next_turn():
	update_datetime_label()
	update_stats_view()
	unlock_turn_button()


func update_datetime_label():
	var daylen_in_turns = root.day_length_in_turns
	var turn_no = root.current_turn_no
	
	var floating_day = float(turn_no) / daylen_in_turns
	floating_day += (1.0/3)	# Offset dusk time by 8hrs
	
	var day_percent = floating_day - floor(floating_day)
	
	var day = floor(floating_day) + 1
	var hours = floor(day_percent * 24)
	var minutes = floor(day_percent * (24 * 60) - hours * 60)
	
	$Control/Datetime/Label.text = "Day %d, %02d:%02d (turn %d)" % [day, hours, minutes, turn_no]


func update_stats_view():
	var clan_id = root.current_clan
	var clan = clan_mgr.get_clan(clan_id) as Clan
	
	if clan != null:
		$Control/Stats/Stone.text		= "%.1f" % clan.resources.stone
		$Control/Stats/Ore.text			= "%.1f" % clan.resources.ore
		$Control/Stats/Plant.text		= "%.1f" % clan.resources.plant
		$Control/Stats/Animal.text		= "%.1f" % clan.resources.animal
		$Control/Stats/Metal.text		= "%.1f" % clan.resources.metal
		$Control/Stats/Commod.text		= "%.1f" % clan.resources.commod
		$Control/Stats/Weapon.text		= "%.1f" % clan.resources.weapon
		$Control/Stats/Knowledge.text	= "%.1f" % clan.resources.knowledge
	
	$Control/Saveload.visible = multiplayer.is_server()


func end_turn():
	# Note: we must lock button BEFORE proccessing turn
	lock_turn_button()
	root.we_done_our_turn()
	
	$Control/EndTurn.release_focus()

func lock_turn_button():
	$Control/EndTurn.text = "Waiting players"
	$Control/EndTurn.disabled = true

func unlock_turn_button():
	$Control/EndTurn.text = "End turn"
	$Control/EndTurn.disabled = false


func show_cell_menu(cell: Cell):
	in_gui = true
	selected_cell = cell
	$CellMenu.visible = true
	$CellMenu.switch_cell(cell)
	
	saved_gpos = global_position
	saved_zoom = $CamBase/CamHead/Camera3D.position.z
	
	get_tree().create_tween().tween_property(self, "global_position", cell.global_position, 0.5)
	get_tree().create_tween().tween_property($CamBase/CamHead/Camera3D, "position:z", zoom_min, 0.5)


func hide_cell_menu():
	in_gui = false
	$CellMenu.visible = false
	$CellSelectCooldown.start()
	
	get_tree().create_tween().tween_property(self, "global_position", saved_gpos, 0.5)
	get_tree().create_tween().tween_property($CamBase/CamHead/Camera3D, "position:z", saved_zoom, 0.5)


func claim_selected_cell():
	var clan = root.current_clan
	
	if selected_cell != null:
		selected_cell.set_cell_owner(clan)
		update_stats_view()


func show_clans_list():
	in_gui = true
	$Control/ClansList.visible = true
	
	var out = "";
	for clan in clan_mgr.get_children():
		out += "[b]%s (id %d)[/b]\n" % [clan.clan_name, clan.clan_id]
		out += "TODO: clan stats here\n"
		out += "\n"
	
	$Control/ClansList/Panel/RichTextLabel.text = out


func hide_clans_list():
	in_gui = false
	$Control/ClansList.visible = false


func _on_button_button_up():
	root.save_state()


func _on_button_2_button_up():
	root.load_state()
