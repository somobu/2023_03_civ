class_name Clan
extends Node

var clan_id = 0;
var clan_name = "RED";
var clan_color = Color.DARK_RED;

var internal_owner_id = -1

var resources: Resources = Resources.new()


func get_state() -> Dictionary:
	var state = {}
	state["name"] = clan_name
	state["color"] = clan_color.to_html(false)
	state["res"] = resources.to_dict()
	
	return state


func set_state(state: Dictionary):
	clan_name = state["name"]
	clan_color = Color.from_string(state["color"], Color.RED)
	resources.from_dict(state["res"])

