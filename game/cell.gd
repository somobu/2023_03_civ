class_name Cell
extends StaticBody3D

const district_ref = preload("res://district/district.tscn")


# Model/logic
var cell_id = -1
var cell_name = "???"
var cell_owner = -1

var del_countdown = -1
var pops = []

# UI and visuals
var hovered = false

@onready var root = get_node("/root/Root") as Root
@onready var player = get_node("/root/Root/Player")
@onready var clan_mgr = get_node("/root/Root/ClanMgr")


func _ready():
	var mesh = $Mesh as MeshInstance3D
	var mat_clone = mesh.mesh.surface_get_material(0).duplicate() as StandardMaterial3D
	mesh.set_surface_override_material(0, mat_clone)
	update_owner_color()
	
	root.connect("next_turn", on_turn_ended)
	
	set_lod_level(player.current_lod_level)
	
	$InfoBillboard.hide()


# State

func get_state() -> Dictionary:
	var state = {}
	state["position"] = "%.3f:%.3f:%.3f" % [position.x, position.y, position.z]
	state["name"] = cell_name
	state["owner"] = cell_owner
	state["cd_del"] = del_countdown
	
	var districts = {}
	for district in get_districts():
		var idx = int((district.name as String).substr(1))
		districts[idx] = district.get_state()
	
	state["dists"] = districts
	
	return state

func set_state(state: Dictionary):
	var pos = (state["position"] as String).split(":")
	position = Vector3(float(pos[0]), float(pos[1]), float(pos[2]))
	
	cell_name = state["name"]
	cell_owner = int(state["owner"])
	del_countdown = int(state["cd_del"])
	
	var districts = state["dists"]
	for dist_idx in districts:
		var district = district_ref.instantiate()
		district.name = "D%d" % int(dist_idx)
		add_child(district)
		district.set_state(districts[dist_idx])
	
	update_owner_color()
	update_info_billboard()


# Visuals

func update_owner_color():
	var mesh = $Mesh as MeshInstance3D
	var mat = mesh.get_surface_override_material(0) as StandardMaterial3D
	mat.albedo_color = get_owner_color()

func update_info_billboard():
	var text = ""
	
	if del_countdown >= 0:
		text = "x_x " + str(del_countdown)
	
	$InfoBillboard.text = text
	$InfoBillboard.visible = text.length() > 0

func show_hover(is_show = true):
	hovered = is_show
	$Outilne.visible = is_show
	
	if is_show:
		get_parent().last_hovered_cell = self
	elif get_parent().last_hovered_cell == self:
		get_parent().last_hovered_cell = null
	
	update_info_billboard()

func set_lod_level(level: int):
	for district in get_districts():
		district.set_lod_level(level)


# Other

func on_turn_ended():
	update_info_billboard()


func get_owner_name():
	if cell_owner < 0: 
		return "free"
	else: 
		var clan = clan_mgr.get_clan(cell_owner) as Clan
		return clan.clan_name


func get_owner_color():
	if cell_owner < 0: 
		return Color.LIGHT_GRAY
	else: 
		var clan = clan_mgr.get_clan(cell_owner) as Clan
		return clan.clan_color


func describe_short():
	return cell_name + "\n" + get_owner_name() + ", " + str(get_districts_count()) + " districts"


func describe_contents():
	var text = ""
	
	text += "Districts:\n"
	for d in get_districts():
		text += " - " + d.get_district_name() + "\n"
	
	return text


func workaround_queue_free():
	var mesh = $Mesh as MeshInstance3D
	mesh.set_surface_override_material(0, null)


# Districts

func init_districts(districts_count = 1):
	var allowed_types = get_natural_dist_types()
	
	for i in range(districts_count):
		var district_type = allowed_types[randi_range(0, allowed_types.size()-1)]
		
		var district = district_ref.instantiate()
		district.name = "D%d" % [i+1]
		district.type = district_type
		add_child(district)
		
		district.enable_all_tasks()
		district.calc_visuals()
	
	position_districts()
	update_info_billboard()

func get_natural_dist_types() -> Array:
	var data = []
	
	for dist_key in root.cfg_districts:
		var dist = root.cfg_districts[dist_key]
		if dist["natural"]:
			data.push_back(dist_key)
	
	return data

func get_districts():
	var districts = []
	for child in get_children():
		if child is District:
			districts.push_back(child)
	return districts

func get_district(idx: int) -> District:
	return get_districts()[idx]

func get_districts_count():
	var count = 0
	for child in get_children():
		if child is District:
			count += 1
	return count

func position_districts():
	var districts = get_districts()
	var dist_count = districts.size()
	
	var circular_dist_count = (dist_count - 1)
	var circular_angle = randf_range(0, 2*PI)
	var degree_step = 1.0 if circular_dist_count == 0 else (2*PI / circular_dist_count)
	
	for i in range(dist_count):
		if i >= 1:
			var angle = (i - 1) * (-degree_step) + circular_angle
			var pos = Vector2(0, 0.3).rotated(angle)
			districts[i].position = Vector3(pos.x, 0, -pos.y)

func change_district(_id: int, _to_type: int):
	assert(false, "Cell.change_district(): unimplemented")


# Resource stats

func get_hold_cost_stats() -> Resources:
	var res = Resources.new()
	
	for district in get_districts():
		res.add(district.get_hold_cost())
	
	return res

func get_overall_production_stats() -> Resources:
	var res = Resources.new()
	
	for district in get_districts():
		res.add(district.get_production_stats())
	
	return res


# Population

func get_pops_limit():
	return 20

func get_pops_count_total():
	return 24

func get_civils_count_total():
	return 12

func get_civils_count_free():
	return 3

func get_mils_count_dict():
	var dict = {}
	for i in range(2):
		dict[i] = randi_range(1, 21)
	return dict


# Cell deletion countdown

func set_deletion_countdown(turns: int):
	del_countdown = turns
	update_info_billboard()

func get_deleteion_countdown():
	return del_countdown

func tick_del_countdown():
	if del_countdown > 0:
		del_countdown -= 1

func will_be_deleted():
	return del_countdown >= 0

func delete_me_now():
	return del_countdown == 0


# Ownership

func is_unowned():
	return cell_owner == -1

func get_owner_id():
	return cell_owner

func is_owned_by(clan: int):
	return cell_owner == clan

func set_cell_owner(new_cell_owner):
	cell_owner = new_cell_owner
	
	var mesh = $Mesh as MeshInstance3D
	var mat = mesh.get_surface_override_material(0) as StandardMaterial3D
	mat.albedo_color = get_owner_color()

func is_losing_owner():
	push_warning("cell.is_losing_owner: unimplemented!")
	return false


# Conflict

func is_in_conflict():
	push_warning("cell.is_in_conflict: unimplemented!")
	return false


# UI

func get_overall_production_stats_ui() -> Resources:
	var res = Resources.new()
	
	for district in get_districts():
		res.add(district.get_production_stats_ui())
	
	return res
